# Header and Emphasis
---
#### For plain Markdown code :

###Header
##### Add "#" in front of your title, at most 6 "#"s
######Example Code:
```
# H1
## H2
### H3
#### H4

```
######Result:
# H1
## H2
### H3
#### H4


### Emphasis

###### Code:
```
* If you want your words to become italics
*asterisks* or _underscores_.

* If you want your words to become bold
**asterisks** or __underscores__.

* Combine both 
**asterisks and _underscores_**.

* If you want to cross out your words 
~~two tildes~~
```
###### Result:
* If you want your words to become italics
*asterisks* or _underscores_.

* If you want your words to become bold
**asterisks** or __underscores__.

* Combine both 
** _asterisks and underscores_**.

* If you want to cross out your words 
~~two tildes~~

#### For Gitbook Editor, simply press some button to do the above functions :

![](/assets/螢幕快照 2019-01-09 上午11.07.33.png)



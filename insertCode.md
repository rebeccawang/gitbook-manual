# Insert Code
---
#### For plain Markdown code :
To make codes **syntax highlight** like below, use " ``` " or "Ctrl + Shift + 9" to wrap your code.

Javascript:
```javascript
var s = "JavaScript syntax highlighting";
alert(s);
```
Python:
```python
s = "Python syntax highlighting"
print s
```

**If no language indicated, no syntax highlighting.**
Just throw the name of language you paste after " ``` " like below.

```
```javascript
var s = "JavaScript syntax highlighting";
alert(s);
``````

```
```python
s = "Python syntax highlighting"
print s
``````

#### For Gitbook Editor, simply press a button to insert code :
![](/assets/螢幕快照 2019-01-09 上午11.12.02.png)
Code syntex parameter: see [here](https://en.support.wordpress.com/code/posting-source-code/)





















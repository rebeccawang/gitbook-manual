# Edit Summary

## Add new chapter
------------------------------

### First Step

##### Add a new article under TOC, and type in your article name.
![](/assets/螢幕快照 2019-01-08 下午3.18.27.png)

### Second Step

##### Click in your newly created article, and the editor will automatically create a new file for you in the FILES section (next to TOC). The default file name will be the same as your article name. You can change the name afterwards.
###### You must click in your newly created article to create a .md file automatically, as in the below example.
![](/assets/螢幕快照 2019-01-09 上午9.03.16.png)
![](/assets/螢幕快照 2019-01-09 上午9.03.10.png)

### Third Step

##### Go to the FILES and click on SUMMARY.md

![](/assets/螢幕快照 2019-01-08 下午3.19.15.png)

### Fourth Step

###### Press the "?" sign on the bottom right corner and select Edit Markdown

![](/assets/螢幕快照 2019-01-08 下午3.22.29.png)
### Fifth Step

##### Type in your newly added article name, and remember to include an url to which page you want to link.
###### URL: The ( ) part behind your [article name]

![](/assets/螢幕快照 2019-01-08 下午3.18.41.png)

###Tips
#####If you want to have subsections like this
![](/assets/螢幕快照 2019-01-10 上午9.09.09.png) 

#####you need to leave a blank in front of your subsections, for example:


```

* [Introduction](README.md)
* [Start Gitbook](/startGitbook.md)
* [Start Gitbook Editor](/startEditor.md)
* [Edit Summary](/editSummary.md)
* [Gitbook markdown](gitbookMarkdown.md)
  * [Header and Emphasis](/HeaderEmphasis.md)
  * [List and Table](/ListTable.md)
  * [Insert Code](/insertCode.md)
  * [Insert Photo](/insertPhoto.md)


```






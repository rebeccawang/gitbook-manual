# Start Gitbook from Scratch 

## First Step

1. Click New project
![](/assets/1.png)
2. Click on the **Import project** tab:
![](/assets/2.png)
3. Copy the Example Repository from GitHub

    In the Git repository URL field, enter the following GitHub repository URL:
    ```
    https://github.com/do-community/hello_hapi.git
    ```   
    It should look like this:
    ![](/assets/3.png)
4. Click **Create project**.
    
## Second Step

Edit to your ".gitlab-ci.yml" file. 
Delete the whole content in ".gitlab-ci.yml" and paste this below as yours.
This script can produce pdf file we want.

    ```bash
    image: billryan/gitbook:latest #fellah/gitbook:latest
    
    pages:
    stage: deploy
    script:
    - gitbook build .
    - gitbook pdf ./
    only:
    - master # this job will affect only the 'master' branch
    tags:
    - gitbook
    artifacts:
    paths:
    - book.pdf
    expire_in: 1 week
    ```

## Third Step

Install the GitLab CI Runner Service

To install the gitlab runner, enter this:
    
    $`curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh -o /tmp/gl-runner.deb.sh`

## Fourth Step

Set Up a GitLab Runner

#### First in Gitlab
    
1. Click CI/CD in setting.
![](/assets/49719626_596819810776872_1544877200677797888_n.jpg)
2. Expand the runners settings.
![](/assets/49741318_310220176283418_8214830152870264832_n.jpg)
   


#### Then in your terminal
1. Pull the Docker Image\(`billryan/gitbook:latest`\)
    
    `$ docker pull billryan/gitbook`

2. Register a GitLab CI Runner with the GitLab Server
            
    Enter this:
    `$ sudo gitlab-runner register`

    You will be asked a series of questions to configure the runner:
        
    * Please enter the gitlab-ci coordinator URL
        
        Type: `http://10.36.94.101/`
        
    * Please enter the gitlab-ci token for this runner
        
        Type your token. (You can see it in the below picture, this is just an example, use **your** token)
        ![](/assets/5.png)
  
    * Please enter the gitlab-ci description for this runner
        
        A name for this particular runner. This will show up in the runner service's list of runners on the command line and in the GitLab interface.
        
    * Please enter the gitlab-ci tags for this runner \(comma separated\)
    
        Type: `Gitbook`
        
    * Please enter the executor (the method used by the runner to complete jobs)
        
        Type: `Docker`
        
    * Please enter the default Docker image**
        
        Type:`billryan/gitbook:latest`
        
#### Finally, back to your gitlab.

        
View the CI/CD Run in GitLab
        
New any file in your project as a test.
    
* If **SUCCESS**, you could see the status of the GitLab CI run:
![](/assets/4.png)
* If **NOT**, you would see pending or fail.
    
    You can check
    
    * your Docker Image works or not.
    * your proxy of docker(check: [HERE](https://faca.gitbook.io/tool/ci/docker/proxy-configuration))
    
    
## Fifth Step
 Now you can use Gitbook Editor to continue.
 Find more about Gitbook Editor in the next chapter.





































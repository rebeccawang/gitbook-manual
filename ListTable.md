#List and  Table
---


## List
##### Adding the plus, munius, or asterisks sign will generate an unordered list
###### Example Code:
```
- Item 1
* Item 2
+ Item 3
```
###### Result:
- Item 1
* Item 2
+ Item 3

##### Adding numbers in front of your list will generate an ordered list
###### Example Code:

```
1. First ordered list item
2. Another item
3. Third item
```
###### Result:
1. First ordered list item
2. Another item
3. Third item

##### Or you can mix them
##### If you have a sub-list, remember to leave a blank in front of your items, as in the below example:
###### Example Code:
```
1. This is a new list
 * one
 * two
2. Try to type one yourself
3. A cool list
```
###### Result:
1. This is a new list
 * one
 * two
2. Try to type one yourself
3. A cool list

## Table


###### Example code:

```
| Fruits        | Are           | Cool  |
| ------------- | ------------- | ------|
| apple         | 1000          | $1600 |
| orange        | 2344          | $12   |
| banana        | 2             | $1    |
```

###### Result:

| Fruits        | Are           | Cool  |
| ------------- | ------------- | ----- |
| apple         | 1000          | $1600 |
| orange        | 2344          | $12   |
| banana        | 2             | $1    |


#####There must be at least **3 dashes** separating each header cell.
#####The outer pipes (|) are optional, and you don't need to make the raw Markdown line up, in other words, you don't need to align the lines.

######  Example Code:
```
Markdown | Less | Pretty
--- | --- | ---
*Still* | `renders` | **nicely**
1 | 2 | 3
```
######  The result:
Markdown | is | Cool
--- | --- | ---
*Still* | `renders` | **nicely**
1 | 2 | 3





# Gitbook Manual

This book is for using Gitbook with Gitlab and Gitbook Editor. 

There are two special files in every Gitbook, README.md and SUMMARY.md.

* README.md usually introduces the abstract of the book, and its content will be shown on the landing page. For example, in this project, README.md generates the Introduction.

* SUMMARY.md defines the tree structure of all chapters, and its format is written with the Markdown nested list. See more in chapter "Edit Summary".




### Table of Contents
* [Start Gitbook](/startGitbook.md)
* [Start Gitbook Editor](/startEditor.md)
* [Edit Summary](/editSummary.md)
* [Gitbook markdown](gitbookMarkdown.md)
  * [Header and Emphasis](/HeaderEmphasis.md)
  * [List and Table](/ListTable.md)
  * [Insert Code](/insertCode.md)
  * [Insert Photo](/insertPhoto.md)





